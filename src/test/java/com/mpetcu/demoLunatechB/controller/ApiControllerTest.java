package com.mpetcu.demoLunatechB.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpetcu.demoLunatechB.model.Dimension;
import com.mpetcu.demoLunatechB.model.Product;
import com.mpetcu.demoLunatechB.service.ProductService;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ApiController.class)
public class ApiControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    private List<Product> productsListSample = Arrays.asList(
        new Product(
            3333,
            123456789,
            "Test name",
            24.50F,
            "Test description",
            40.30F,
            true,
            new Dimension(
                20.10F,
                 22.10F,
                24.10F
            )
        ),
        new Product(
            3334,
                123456790,
                "Test name #2",
                30.20F,
                "Test description #2",
                41.50F,
                true,
                new Dimension(
                10.50F,
                 40.10F,
                22.10F
            )
        )
    );

    @Test
    public void getProductsByDescription_simpleGetRequest_mapWithProductListAndTotal() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("products", this.productsListSample);
        map.put("totalWeight", 65.00);
        ObjectMapper objectMapper = new ObjectMapper();

        Mockito
            .when(this.productService.getAllProductsByDescriptionAndTotal("chair"))
            .thenReturn(map);

        MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/get-products-by-description/chair")).andReturn().getResponse();

        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(objectMapper.writeValueAsString(map), response.getContentAsString());
    }

    @Test
    public void topExpensiveProducts_simpleGetReques_listWithTopExpensiveProducts() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Mockito
            .when(productService.getMostExpensiveProducts(2))
            .thenReturn(this.productsListSample);

        MockHttpServletResponse response = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/top-expensive-products/2")).andReturn().getResponse();

        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(objectMapper.writeValueAsString(this.productsListSample), response.getContentAsString());
    }

    @Test
    public void isProductsServiceAvailable_simpleGetRequest_returnProperMessage() throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, String> responseFalseSample = new HashMap<>();
        responseFalseSample.put("mesage", "Content parser is running. Content may be unavailable or incomplete.");
        responseFalseSample.put("status", "limited");

        Map<String, String> responseTrueSample = new HashMap<>();
        responseTrueSample.put("message", "Content is fully available.");
        responseTrueSample.put("status", "available");

        Mockito
            .when(productService.isSetupComplete())
            .thenReturn(false);

        MockHttpServletResponse response1 = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/service-status")).andReturn().getResponse();

        Assert.assertEquals(200, response1.getStatus());
        Assert.assertEquals(objectMapper.writeValueAsString(responseFalseSample), response1.getContentAsString());

        Mockito
            .when(productService.isSetupComplete())
            .thenReturn(true);

        MockHttpServletResponse response2 = this.mockMvc.perform(MockMvcRequestBuilders.get("/api/service-status")).andReturn().getResponse();

        Assert.assertEquals(200, response2.getStatus());
        Assert.assertEquals(objectMapper.writeValueAsString(responseTrueSample), response2.getContentAsString());
    }

}