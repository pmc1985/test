package com.mpetcu.demoLunatechB.service;

import com.mpetcu.demoLunatechB.model.Product;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImportProductsServiceTest {

    @InjectMocks
    @Autowired
    private ImportProductsService importProductsService;

    @Mock
    private ProductService productService = null;

    @Test
    public void checkIfLunaFactoryApiIsRespondingWith200() {
        try {
            ResponseEntity<List<Product>> actual = importProductsService.parsePage(1);
            Assert.assertEquals("Response from LunaFactory API is not as expected", HttpStatus.OK, actual.getStatusCode());
        }catch(Exception e) {
            Assert.assertTrue("Conuld not connect to LunaFactory API: " + e.getMessage(), false);
        }
    }
}