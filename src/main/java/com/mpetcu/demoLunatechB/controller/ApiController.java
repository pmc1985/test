package com.mpetcu.demoLunatechB.controller;

import com.mpetcu.demoLunatechB.model.Product;
import com.mpetcu.demoLunatechB.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ProductService productService;

    @CrossOrigin
    @ApiOperation( value = "Will return a map containing the complete list of products and total weight of products. All product will match the given parameter as product description." )
    @ApiResponses(value = @ApiResponse(
        code = 200,
        message = "A map containing a list (of products) and an integer (sum of products weight)"
    ))
    @RequestMapping(
        value = "/get-products-by-description/{descriptionQueryString}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET
    )
    public ResponseEntity<?> getProductsByDescription(
        @ApiParam(value = "The provided string should match exactly the product description field.", required = true)
        @PathVariable("descriptionQueryString")
        String descriptionQueryString){
        Map<String, Object> productAndTotal = productService.getAllProductsByDescriptionAndTotal(descriptionQueryString);
        return new ResponseEntity<>(productAndTotal, HttpStatus.OK);
    }

    @CrossOrigin
    @ApiOperation( value = "Will return a list of products with length defined by given parameter." )
    @ApiResponses(value = @ApiResponse(
        code = 200,
        message = "List of products"
    ))
    @RequestMapping(
        value = "/top-expensive-products/{size}",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET
    )
    public ResponseEntity<?> topExpensiveProducts(
        @PathVariable("size")
        int size){
        List<Product> products = productService.getMostExpensiveProducts(size);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @CrossOrigin
    @ApiOperation( value = "Will return a message and status with service availability" )
    @ApiResponses(value = @ApiResponse(
        code = 200,
        message = "Service availability message"
    ))
    @RequestMapping(
        value = "/service-status",
        produces = MediaType.APPLICATION_JSON_VALUE,
        method = RequestMethod.GET
    )
    public ResponseEntity<?> isProductsServiceAvailable(){
        Map<String, String> message= new HashMap<>();
        if(productService.isSetupComplete()) {
            message.put("message", "Content is fully available.");
            message.put("status", "available");
        }else{
            message.put("mesage", "Content parser is running. Content may be unavailable or incomplete.");
            message.put("status", "limited");
        }
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
