package com.mpetcu.demoLunatechB.controller;

import com.mpetcu.demoLunatechB.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    @Autowired
    ProductService productService;

    @RequestMapping({"","/"})
    public String index(){
        return "index";
    }

    @RequestMapping("/all-by-description")
    public String tab1(@RequestParam(value = "q", defaultValue = "chair", required = false) String q, Model model){
        model.addAttribute("isSetupComplete", productService.isSetupComplete());
        model.addAttribute("q", q);
        model.addAllAttributes(
            productService.getAllProductsByDescriptionAndTotal(q)
        );
        return "tab1";
    }

    @RequestMapping("/most-expensive")
    public String tab2(@RequestParam(value = "size", defaultValue = "15", required = false) String size, Model model){
        model.addAttribute("isSetupComplete", productService.isSetupComplete());
        try{
            int intSize = Integer.parseInt(size);
            if(intSize < 0 || intSize > 100 ){
                throw new NumberFormatException();
            }
            model.addAttribute("size", intSize);
            model.addAttribute("products", productService.getMostExpensiveProducts(intSize));

        }catch (NumberFormatException e){
            model.addAttribute("exception", "Size of list must be a number in range 1-100.");
        }
        return "tab2";
    }


}
