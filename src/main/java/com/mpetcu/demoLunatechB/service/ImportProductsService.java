package com.mpetcu.demoLunatechB.service;

import com.mpetcu.demoLunatechB.config.ImporterApiConfigProperties;
import com.mpetcu.demoLunatechB.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class ImportProductsService {

    @Autowired
    private ImporterApiConfigProperties importerApiConfigProperties;

    @Autowired
    private ProductService productService;

    private static boolean running = true;

    private RestTemplate restTemplate = new RestTemplate();

    public static boolean isRunning() {
        return running;
    }

    public String getUrl(int page){
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
            .fromUriString(this.importerApiConfigProperties.getUrl())
            .queryParams(this.importerApiConfigProperties.getUrlParams(page));
        return uriBuilder.toUriString();
    }

    public HttpEntity<String> getRequest(){
        HttpHeaders headers = new HttpHeaders();
        headers.set(
            importerApiConfigProperties.getHeader().get("key"),
            importerApiConfigProperties.getHeader().get("val")
        );
        return new HttpEntity<>(null, headers);
    }

    public ResponseEntity<List<Product>> parsePage(@NotNull int page) {
        return
            this.restTemplate.exchange(
                this.getUrl(page),
                HttpMethod.GET,
                this.getRequest(),
                new ParameterizedTypeReference<List<Product>>(){}
            );
    }

    @Scheduled(fixedRateString = "${importer.scheduler.frequency}", initialDelayString = "${importer.scheduler.delay}" )
    public void parse() throws InterruptedException {
        List<Product> products;
        int page = 0;
        do {
            products = this.parsePage(page++).getBody();
            productService.saveAll(products);
            TimeUnit.SECONDS.sleep(6);
        } while ( products.size() > 0);
        running = false;
    }

}
