package com.mpetcu.demoLunatechB.service;

import com.mpetcu.demoLunatechB.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Map<String, Object> getAllProductsByDescriptionAndTotal(String descriptionQueryString){

        Map<String, Object> productsAndTotal = new HashMap<>();
        List<Product> products = productRepository.findAllByDescription(descriptionQueryString);

        Double total = products
                            .stream()
                            .mapToDouble(product -> product.getWeight())
                            .sum();

        productsAndTotal.put("products", products);
        productsAndTotal.put("totalWeight", total);

        return productsAndTotal;
    }

    public List<Product> getMostExpensiveProducts(int size){
        return productRepository.findAllByOrderByPriceDesc(PageRequest.of(0, size));
    }

    public void saveAll( List<Product> productCollection ){
        productRepository.saveAll(productCollection);
    }

    public boolean isSetupComplete(){
        return !ImportProductsService.isRunning();
    }
}
