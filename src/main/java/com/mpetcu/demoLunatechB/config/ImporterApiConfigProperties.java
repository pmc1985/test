package com.mpetcu.demoLunatechB.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "importer.api")
public class ImporterApiConfigProperties {
    private String url;
    private Map<String, String> header;
    private MultiValueMap<String, String> urlParams;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public MultiValueMap<String, String> getUrlParams() {
        return urlParams;
    }

    public MultiValueMap<String, String> getUrlParams(Integer page) {
        this.urlParams.replace("page", Arrays.asList(page.toString()));
        return urlParams;
    }

    public void setUrlParams(MultiValueMap<String, String> urlParams) {
        this.urlParams = urlParams;
    }
}
