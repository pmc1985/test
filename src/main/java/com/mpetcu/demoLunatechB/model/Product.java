package com.mpetcu.demoLunatechB.model;

import org.springframework.data.annotation.Id;

public class Product {

    @Id
    private int id;
    private int ean;
    private String name;
    private float weight;
    private String description;
    private float price;
    private boolean assembled;
    private Dimension dimension;

    public Product() {
    }

    public Product(int id, int ean, String name, float weight, String description, float price, boolean assembled, Dimension dimension) {
        this.id = id;
        this.ean = ean;
        this.name = name;
        this.weight = weight;
        this.description = description;
        this.price = price;
        this.assembled = assembled;
        this.dimension = dimension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEan() {
        return ean;
    }

    public void setEan(int ean) {
        this.ean = ean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isAssembled() {
        return assembled;
    }

    public void setAssembled(boolean assembled) {
        this.assembled = assembled;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }
}
