package com.mpetcu.demoLunatechB.model;

public class Dimension {

    private float depth;
    private float width;
    private float height;

    public Dimension(float depth, float width, float height) {
        this.depth = depth;
        this.width = width;
        this.height = height;
    }

    public Dimension() {
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}
