package com.mpetcu.demoLunatechB;

import io.swagger.annotations.Api;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableScheduling
@EnableSwagger2
public class DemoLunatechBApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLunatechBApplication.class, args);
	}

	@Bean
	public Docket setup(){
		return new Docket(DocumentationType.SWAGGER_2)
			.select()
			.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
			.paths(PathSelectors.any())
			.build()
			.apiInfo(new ApiInfo(
				"Rest API for LUNATECH beginner test",
				"One Controller API for educational purpose. \nDescriptions are defined on each request method below :).",
				"1.0",
				null,
				new Contact("Mihai Petcu", null, "mihai.costin.petcu@gmail.com"),
				null,
				null,
				Collections.emptyList()
			));
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
}
